import {Component, OnInit} from '@angular/core';
import {Product} from '../../models/product';
import {ProductService} from '../../service/product.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenService} from '../../service/token.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products: Product[] = [];
  roles: string[];
  isAdmin = false;

  constructor(
    private service: ProductService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private tokenService: TokenService) {
  }

  ngOnInit(): void {
    this.loadProduct();
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol => {
      if (rol === 'ROLE_ADMIN') {
        this.isAdmin = true;
      }
    });
  }

  loadProduct(): void {
    this.service.list().subscribe(
      data => {
        this.products = data;
      }, err => {
        console.log(err);
      }
    );
  }

  remove(id: number) {
    this.service.delete(id).subscribe(
      data => {
        this.toastr.success('Product removed', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.loadProduct();
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
      }
    );
  }
}
