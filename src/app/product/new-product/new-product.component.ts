import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../service/product.service';
import {Product} from '../../models/product';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {
  name = '';
  price = null;

  constructor(
    private service: ProductService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  onCreate(): void {
    const product = new Product(this.name, this.price);
    this.service.save(product).subscribe(
      data => {
        this.toastr.success('Product created', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/list']);
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
        // this.router.navigate(['/']);
      });
  }

}
