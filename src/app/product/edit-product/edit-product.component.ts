import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../service/product.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../../models/product';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product: Product = null;

  constructor(
    private service: ProductService,
    private toastr: ToastrService,
    private activateRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    const id = this.activateRoute.snapshot.params.id;
    this.service.detail(id).subscribe(
      data => {
        this.product = data;
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/']);
      });
  }

  onUpdate(): void {
    const id = this.activateRoute.snapshot.params.id;
    this.service.update(id, this.product).subscribe(
      data => {
        this.toastr.success('Product updated', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/list']);
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
        // this.router.navigate(['/']);
      });
  }
}
