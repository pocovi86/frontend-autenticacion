import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewUser} from '../models/new-user';
import {Observable} from 'rxjs';
import {LoginUser} from '../models/login-user';
import {JwtDTO} from '../models/jwt-dto';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authURL = 'http://localhost:8080/auth/';

  constructor(private httpCliente: HttpClient) { }

  public new(newUser: NewUser): Observable<any>{
    return this.httpCliente.post<any>(this.authURL + 'new', newUser);
  }

  public login(loginUser: LoginUser): Observable<JwtDTO>{
    return this.httpCliente.post<JwtDTO>(this.authURL + 'login', loginUser);
  }
}
