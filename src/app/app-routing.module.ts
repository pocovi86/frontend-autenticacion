import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListProductComponent} from './product/list-product/list-product.component';
import {DetailProductComponent} from './product/detail-product/detail-product.component';
import {NewProductComponent} from './product/new-product/new-product.component';
import {EditProductComponent} from './product/edit-product/edit-product.component';
import {IndexComponent} from './index/index.component';
import {LoginComponent} from './auth/login/login.component';
import {RecordComponent} from './auth/record/record.component';
import {ProdGuardService as guard} from './guards/prod-guard.service';

const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'login', component: LoginComponent},
  {path: 'record', component: RecordComponent},
  {path: 'list', component: ListProductComponent, canActivate: [guard], data: {expectedRol: ['admin', 'user']}},
  {path: 'detail/:id', component: DetailProductComponent, canActivate: [guard], data: {expectedRol: ['admin', 'user']}},
  {path: 'new', component: NewProductComponent, canActivate: [guard], data: {expectedRol: ['admin']}},
  {path: 'edit/:id', component: EditProductComponent, canActivate: [guard], data: {expectedRol: ['admin']}},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
