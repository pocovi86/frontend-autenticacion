import {Component, OnInit} from '@angular/core';
import {NewUser} from '../../models/new-user';
import {TokenService} from '../../service/token.service';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {LoginUser} from '../../models/login-user';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {
  newUser: NewUser;
  name: string;
  nameUser: string;
  email: string;
  password: string;
  errMsj: string;
  isLogged = false;

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
    }
  }

  onRegister(): void {
    this.newUser = new NewUser(this.name, this.nameUser, this.email, this.password);
    this.authService.new(this.newUser).subscribe(
      data => {
        this.toastr.success('Account created', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});

        this.router.navigate(['/login']);
      }, err => {
        this.errMsj = err.error.message;
        this.toastr.error(this.errMsj, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
      });
  }
}
